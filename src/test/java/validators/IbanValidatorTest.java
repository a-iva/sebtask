package validators;

import config.IbanDetails;
import org.junit.Test;

import java.util.HashMap;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class IbanValidatorTest {

    HashMap<String, Integer> validDetails = IbanDetails.validCountryIbanDetails();
    IbanValidator validator = new IbanValidator();

    @Test
    public void shouldFailLengthTooShort() {
        String iban = "LV000001";
        boolean isValid = validator.validate(iban, validDetails);
        assertFalse(isValid);
    }

    @Test
    public void shouldFailLengthTooLong() {
        String iban = "LV0000000000000000000001";
        boolean isValid = validator.validate(iban, validDetails);
        assertFalse(isValid);
    }

    @Test
    public void shouldFailInvalidCountryCode() {
        String iban = "AA123456789012345678";
        boolean isValid = validator.validate(iban, validDetails);
        assertFalse(isValid);
    }

    @Test
    public void shouldFailLengthOkModInvalid() {
        String iban = "LV80BANK0000435195002";
        boolean isValid = validator.validate(iban, validDetails);
        assertFalse(isValid);
    }

    @Test
    public void shouldPassLengthOkModOk() {
        String iban = "LV80BANK0000435195001";
        boolean isValid = validator.validate(iban, validDetails);
        assertTrue(isValid);
    }



}