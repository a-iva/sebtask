package config;

import java.util.HashMap;

public class IbanDetails {

    public static HashMap<String, Integer> validCountryIbanDetails() {
        HashMap<String, Integer> validDetails = new HashMap();
        validDetails.put("LT", 20);
        validDetails.put("LV", 21);
        validDetails.put("EE", 20);
        validDetails.put("NL", 18); //Netherlands
        validDetails.put("GB", 22); //Great Britain
        validDetails.put("GE", 22); //Germany

        return validDetails;
    }
}
