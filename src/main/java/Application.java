import services.FileIbanValidationService;
import services.IExecutable;
import services.SingleIbanValidationService;
import validators.IbanValidator;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * The application to check validity of IBAN codes.
 * Currently available options are:
 * 1. single IBAN check
 * 2. multiple IBAN check from a file
 */
public class Application {

    public static void main(String[] args) {

        IbanValidator validator = new IbanValidator();
        Map<Integer, IExecutable> commands = new HashMap();

        commands.put(1, new SingleIbanValidationService(validator));
        commands.put(2, new FileIbanValidationService(validator));

        while (true) {
            printMenu();
            int menuItem = getFromUserMenuItemToExecute();
            if (menuItem == 3) {
                break;
            }

            IExecutable service = commands.get(menuItem);
            service.execute();
        }
    }

    private static void printMenu() {
        System.out.println("Available options:");
        System.out.println("1. Validate single IBAN");
        System.out.println("2. Validate IBAN(s) from file");
        System.out.println("3. Exit");
    }

    private static int getFromUserMenuItemToExecute() {
        System.out.print("Please choose an option:");
        Scanner sc = new Scanner(System.in);
        return Integer.parseInt(sc.nextLine());
    }
}
