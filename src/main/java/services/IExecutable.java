package services;

public interface IExecutable {

    void execute();
}
