package services;

import config.IbanDetails;
import org.apache.log4j.Logger;
import validators.IValidator;

import java.io.*;
import java.util.Scanner;

public class FileIbanValidationService implements IExecutable {

    private IValidator validator;
    private Logger log = Logger.getRootLogger();

    public FileIbanValidationService(IValidator validator) {
        this.validator = validator;
    }

    @Override
    public void execute() {
        Scanner scanner = new Scanner(System.in);
        FileWriter writer;
        BufferedWriter bufferedWriter = null;

        System.out.print("enter path to the file: ");
        String filePath = scanner.next();

        try {
            FileReader fileReader = new FileReader(filePath);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String ibanLine;

            writer = new FileWriter(createEmptyOutFile(filePath));
            bufferedWriter = new BufferedWriter(writer);

            while ((ibanLine = bufferedReader.readLine()) != null) {
                //log.debug(ibanLine);

                boolean isIbanValid = validator.validate(ibanLine, IbanDetails.validCountryIbanDetails());
                bufferedWriter.write(ibanLine + ";" + isIbanValid);
                bufferedWriter.newLine();

                //log.debug(ibanLine + ";" + isIbanValid + "\n");
            }
            System.out.println("specified file is parsed!");

        } catch (FileNotFoundException e) {
            System.out.println("the specified file is not found");

        } catch (Exception e) {
            System.out.println("an exception has occurred");

        } finally {
            if (bufferedWriter != null) {
                try {
                    bufferedWriter.close();
                } catch (IOException e) {
                    System.out.println("error encountered while closing buffered writer" + e);
                }
            }
        }
    }

    private File createEmptyOutFile(String filePath) {
        File outFile = new File(filePath+".out");
        if (!outFile.exists()) {
            try {
                outFile.createNewFile();
            } catch (IOException e) {
                System.out.println("error when creating file with name " + filePath + ".out ");
                e.printStackTrace();
            }
        }
        return outFile;
    }
}
