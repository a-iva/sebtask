package services;

import config.IbanDetails;
import validators.IValidator;

import java.util.Scanner;

public class SingleIbanValidationService implements IExecutable {

    private IValidator validator;

    public SingleIbanValidationService(IValidator validator) {
        this.validator = validator;
    }

    @Override
    public void execute() {
        Scanner scanner = new Scanner(System.in);

        System.out.print("enter IBAN: ");
        String iban = scanner.next();

        boolean isValid = validator.validate(iban, IbanDetails.validCountryIbanDetails());
        System.out.println(isValid);
    }
}
