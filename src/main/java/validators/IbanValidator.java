package validators;

import org.apache.log4j.Logger;

import java.math.BigInteger;
import java.util.HashMap;

/**
 * Validator to check whether specified IBAN code is valid.
 * The validity of IBAN code is based on country ISO code and total length of IBAN.
 */
public class IbanValidator implements IValidator {

    private Logger log = Logger.getRootLogger();

    public IbanValidator() {}

    @Override
    public boolean validate(String iban, HashMap<String, Integer> validDetails) {
        if (!isLengthCorrect(iban, validDetails)) {
            return false;
        }

        String rearranged = rearrangeCharacters(iban);
        //log.info("rearranged iban: " + rearranged);
        String converted = convertToInteger(rearranged);
        //log.info("converted string: " + converted);
        BigInteger remainder = computeRemainder(converted);
        //log.info("remainder: " + remainder);

        return remainder.equals(BigInteger.ONE);
    }

    private int getLengthForCountry(String countryCode, HashMap<String, Integer> validDetails) {
        for (String code : validDetails.keySet()) {
            if (code.equals(countryCode)) {
                return validDetails.get(code);
            }
        }
        return -1;
    }

    private boolean isLengthCorrect(String iban, HashMap<String, Integer> validDetails) {
        return iban.length() == getLengthForCountry(iban.substring(0,2), validDetails);
    }

    private String rearrangeCharacters(String iban) {
        iban = iban.substring(4) + iban.substring(0, 4);
        return iban;
    }

    private String convertToInteger(String rearrangedIban) {
        StringBuilder convertedString = new StringBuilder();
        for (char ch : rearrangedIban.toCharArray()) {
            String charToAdd = Character.isLetter(ch) ? String.valueOf(Character.toUpperCase(ch) - 55) : Character.toString(ch);
            convertedString.append(charToAdd);
        }
        return convertedString.toString();
    }

    private BigInteger computeRemainder(String convertedIban) {
        BigInteger convertedIbanInteger = new BigInteger(convertedIban);
        return convertedIbanInteger.mod(BigInteger.valueOf(97));
    }
}
