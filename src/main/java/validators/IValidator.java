package validators;

import java.util.HashMap;

public interface IValidator {

    /**
     * Check whether specified IBAN code is valid.
     * @param iban IBAN code to be checked
     * @param validDetails mapping of all currently valid country codes and corresponding lengths
     * @return true if IBAN code is valid
     */
    boolean validate(String iban, HashMap<String, Integer> validDetails);
}
