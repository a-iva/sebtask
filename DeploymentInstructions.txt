You can download the archive with code and jar file from Downloads section.

Instructions to run application from command line prompt (I am using Windows):
0. download and extract the archive (this step can be done outside command line)
1. change directory to sebtask/target
2. locate the file Sebtask.jar (it should be there)
3. run> java -jar Sebtask.jar 

It is also possible to run the program from IDE if you wish, for that you need to import the existing project and run it.

Note: The available country codes and corresponding lengths of IBAN code per country are stored in java/config/IbanDtails class and are following:
LT - 20 digits, LV - 21 digit, EE - 20 digits, NL - 18 digits, GB - 22 digis, GE - 22 digits.
Please feel free to clone the repository and add another ones if you would like to.

Technologies used:
Java 8, Maven, Log4j for debug output, JUnit4 for Unit tests, IntelliJ IDEA.
Ideally, I would use Spring Core to inject an instance of IbanValidator class into ones using it but I have decided not to do so to avoid massive amount of libraries for small project.